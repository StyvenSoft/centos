#include <stdio.h>
#include <stdlib.h>

int main()
{
    char producto;
    char opcion;
    char categoria;
    int precio;
    int totalA;
    int contadorA;
    int totalB;
    int contadorB;
    int totalC;
    int contadorC;
    int totalglobal;
    int totalCantidad;
    int i;

    totalA = 0;
    contadorA = 0;
    totalB = 0;
    contadorB = 0;
    totalC = 0;
    contadorC = 0;

    do{
        printf("Drogeria CentOS!\n");
        printf("Ingrese nombre del producto vendido: \n");
        scanf("%s",&producto);
        printf("Ingrese la categoria: A, B, C\n");
        scanf("%s",&categoria);
        if(categoria == 'a' || categoria == 'A'){
            do{
                printf("Precios categoria A: 2000 a 80000 \n");
                printf("Ingrese el precio de venta: \n");
                scanf("%i", &precio);
                i= 0;
                if(precio >= 2000 && precio <= 80000 ){
                    printf("Producto agregado Correctamente! \n");
                    totalA = precio + totalA;
                    contadorA = contadorA + 1;
                }else{
                    printf("Precio fuera de rando, intente nuevamente! \n");
                    i++;
                }
           }while(i>=1);
        }
        else if(categoria == 'b' || categoria == 'B'){
            do{
                printf("Precios categoria B: 8000 a 50000 \n");
                printf("Ingrese el precio de venta: \n");
                scanf("%i", &precio);
                i= 0;
                if(precio >= 8000 && precio <= 50000 ){
                    printf("Producto agregado Correctamente! \n");
                    totalB = precio + totalB;
                    contadorB = contadorB + 1;
                }else{
                    printf("Precio fuera de rando, intente nuevamente! \n");
                    i++;
                }
           }while(i>=1);
        }
        else if(categoria == 'c' || categoria == 'C'){
            do{
                i= 0;
                printf("Precios categoria C: 100000 a 200000  \n");
                printf("Ingrese el precio de venta: \n");
                scanf("%i", &precio);

                if(precio >= 100000 && precio <= 200000 ){
                    printf("Producto agregado Correctamente! \n");
                    totalC = precio + totalC;
                    contadorC = contadorC + 1;
                }else{
                    printf("Precio fuera de rando, intente nuevamente! \n");
                    i++;
                }
           }while(i>=1);
        }else{
            printf("Error en la categoria,intente nuevamente! \n");
        }
        fflush(stdin);
        printf("Presione 'Enter' para ver resultados. \n");
        printf("Presione 's' para agregar otro producto. \n");
        scanf("%c",&opcion);

    }while(opcion == 's' || opcion == 'S');

    printf("Cantidad de productos categoria A: %i\n", contadorA);
    printf("Total de venta A: %i\n", totalA);
    printf("Cantidad de productos categoria B: %i\n", contadorB);
    printf("Total de venta B: %i\n", totalB);
    printf("Cantidad de productos categoria C: %i\n", contadorC);
    printf("Total de venta C: %i\n", totalC);
    totalCantidad= contadorA + contadorB + contadorC;
    printf("Cantidad total de productos vendidos: %i\n", totalCantidad);
    totalglobal= totalA + totalB + totalC;
    printf("Total global de ventas: %i\n", totalglobal);

    return 0;
}
